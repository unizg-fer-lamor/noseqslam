#include <NOSeqSLAM.h>
#include <SeqSLAM.h>
#include <descriptors/feature_selection/selection.h>
#include <future>
#include <tensor.h>

void fsExperiment() {

    std::string expName = "fsc";
    std::string dbPath = "../results/fsc/";

    std::vector<std::string> descriptors = {
            "alexnet3",
            "alexnet4",
            "alexnet3places365",
            "alexnet4places365",
            "resnet1817",
            "resnet1817places365",
            "resnet5049",
            "resnet5049places365"
    };

    std::vector<std::pair<std::string, std::string>> trainingAndTestingDatasets = {
            { "bonn", "freiburg" },
            { "bonn", "nordland" },
            { "bonn", "gardenpoint" }
    };

    // create experiments
    std::vector<std::tuple<std::string, std::string, std::string>> experiments{};
    for (const auto& descriptorName: descriptors)
        for (const auto& dataset: trainingAndTestingDatasets)
            experiments.push_back({ dataset.first, dataset.second, descriptorName });

    // create folders
    std::vector<std::string> folders{};
    for (const auto& descriptorName: descriptors)
        for (const auto& dataset: trainingAndTestingDatasets)
            folders.push_back("../results/" + expName + "/" + descriptorName + "/matrices/" + dataset.second + "/");

    std::vector<unsigned int> dsArr = { 3, 5, 31, 43, 51 };
    std::vector<int> etaExpArr = { 2, 3 };

    fs::main(experiments, expName, dbPath);

    std::future<void> noseqf = std::async(noseq::main, folders, dsArr, etaExpArr, expName);
    std::future<void> seqf = std::async(seq::main, folders, dsArr, expName, false);
    std::future<void> seqcf = std::async(seq::main, folders, dsArr, expName, true);

    noseqf.get();
    seqf.get();
    seqcf.get();

    mm::precisionAndRecallExperiment("../results/noseqslam/" + expName + "/matrices/freiburg", "../data/freiburg_example/gt_freiburg_example.out", "../results/precision_and_recall/" + expName + "/");
    mm::precisionAndRecallExperiment("../results/noseqslam/" + expName + "/matrices/nordland", "../data/nordland_example/gt_nordland_example.out", "../results/precision_and_recall/" + expName + "/");
    mm::precisionAndRecallExperiment("../results/noseqslam/" + expName + "/matrices/gardenpoint", "../data/gardenpoint_example/gt_gardenpoint_example.out", "../results/precision_and_recall/" + expName + "/");

    mm::precisionAndRecallExperiment("../results/seqslam/" + expName + "/matrices/freiburg", "../data/freiburg_example/gt_freiburg_example.out", "../results/precision_and_recall/" + expName + "/");
    mm::precisionAndRecallExperiment("../results/seqslam/" + expName + "/matrices/nordland", "../data/nordland_example/gt_nordland_example.out", "../results/precision_and_recall/" + expName + "/");
    mm::precisionAndRecallExperiment("../results/seqslam/" + expName + "/matrices/gardenpoint", "../data/gardenpoint_example/gt_gardenpoint_example.out", "../results/precision_and_recall/" + expName + "/");

    mm::precisionAndRecallExperiment("../results/seqslamcone/" + expName + "/matrices/freiburg", "../data/freiburg_example/gt_freiburg_example.out", "../results/precision_and_recall/" + expName + "/");
    mm::precisionAndRecallExperiment("../results/seqslamcone/" + expName + "/matrices/nordland", "../data/nordland_example/gt_nordland_example.out", "../results/precision_and_recall/" + expName + "/");
    mm::precisionAndRecallExperiment("../results/seqslamcone/" + expName + "/matrices/gardenpoint", "../data/gardenpoint_example/gt_gardenpoint_example.out", "../results/precision_and_recall/" + expName + "/");
}


void softmaxExperiment() {
    // where to store results
    std::string root = "../results/";
    // experiment name
    std::string expName = "softmax_experiment";
    // where feature maps reside
    std::string fmPath = "../../feature_maps/";
    // where the original data reside (in order to obtain g.t.)
    std::string dataPath = "../../data/";
    // where to store precision and recall
    std::string prPath = "../results/precision_and_recall/";


    std::string anyFile{fl::split(fl::listDirectory(fmPath)[0], '/').back()};

    std::function<std::vector<std::string>(const std::vector<std::string>&)> woLast = [](const std::vector<std::string>& v) {
        std::vector<std::string> wo{v};
        wo.pop_back();
        return wo;
    };

    std::function<std::string(const std::string&)> lowercase = [](const std::string& s) {
        std::string l{s};
        std::transform(l.begin(), l.end(), l.begin(), [](unsigned char c){ return std::tolower(c); });
        return l;
    };

    std::string descriptorName{fl::join(woLast(woLast(fl::split(anyFile, '_'))), "~")};
    std::string dataset{lowercase(woLast(fl::split(anyFile, '_')).back())};

    // create corresponding folders for images & assoc. matrices
    std::string imagesFolder{root + expName + "/" + dataset + "/images/"};
    std::string amFolder{root + expName + "/" + dataset + "/matrices/"};
    fl::createDirectory(imagesFolder);
    fl::createDirectory(amFolder);
    // amFolders are all folders that contain association matrices which will be evaluated
    // on vpr algorithms
    std::vector<std::string> amFolders{};
    // add amFolder to amFolders
    amFolders.push_back(amFolder);

    std::string q{fmPath + fl::join(woLast(fl::split(anyFile, '_')), "_") + "_query.pt"};
    std::string d{fmPath + fl::join(woLast(fl::split(anyFile, '_')), "_") + "_reference.pt"};

    // std::cout << descriptorName << "\n" << dataset <<  "\n" << q << "\n" << d << "\n" << amFolder << "\n";

    torch::Tensor queryDB{ts::loadPython(q)};
    queryDB.div_(torch::norm(queryDB, 2, 1, true));

    torch::Tensor refDB{ts::loadPython(d)};
    refDB.div_(torch::norm(refDB, 2, 1, true));

    am::AssociationMatrix m{queryDB.matmul(refDB.transpose(0, 1))};
    m.saveToFile(amFolder + descriptorName + "_" + dataset + ".out");
    m.normalizeColumns();
    img::saveImage(img::associationMatrixDrawing(m), imagesFolder + descriptorName + "_" + dataset + ".png");


    std::vector<unsigned int> dsArr = { 31, 43, 51 };
    std::vector<int> etaExpArr = { 2, 3 };

    std::future<void> noseqf = std::async(noseq::main, amFolders, dsArr, etaExpArr, expName);
    std::future<void> seqf = std::async(seq::main, amFolders, dsArr, expName, false);
    std::future<void> seqconef = std::async(seq::main, amFolders, dsArr, expName, true);

    noseqf.get();
    seqf.get();
    seqconef.get();

    mm::precisionAndRecallExperiment("../results/noseqslam/" + expName + "/matrices/" + dataset, dataPath + dataset + "_example/gt_" + dataset + "_example.out", prPath + expName + "/");
    mm::precisionAndRecallExperiment("../results/seqslam/" + expName + "/matrices/" + dataset, dataPath + dataset + "_example/gt_" + dataset + "_example.out", prPath + expName + "/");
    mm::precisionAndRecallExperiment("../results/seqslamcone/" + expName + "/matrices/" + dataset, dataPath + dataset + "_example/gt_" + dataset + "_example.out", prPath + expName + "/");
}

int main() {
    softmaxExperiment();
    return 0;
}