#ifndef VISUALPLACERECOGNITION_PROB_SELECTION_H
#define VISUALPLACERECOGNITION_PROB_SELECTION_H

#include <torch/torch.h>
#include <vector>
#include <utility/timer.h>
#include "shared.h"


namespace fsp {

    torch::Tensor probability(const torch::Tensor& db) {
        // repeat db in batch dimension
        torch::Tensor m1{db.repeat({db.size(0), 1, 1})};
        // another db transposed in non-batch (i.e. rows&columns) dimensions
        torch::Tensor m2{m1.transpose(0, 1)};
        // L2 norm for one-dimensional feature points
        torch::Tensor aSum{torch::abs(m1 - m2).sum(0)};
        // if some of the features is (0, 0,..., 0), make it an uniform distribution
        torch::Tensor u{aSum.sum(0) == 0.0f};
        u = u.toType(torch::ScalarType::Float);
        u.div_(aSum.size(0));
        u = u.repeat({aSum.size(0), 1});
        aSum.add_(u);
        // 'non-normalized' probability to probability (i.e. divide each probability with its sum)
        torch::Tensor aSumN{aSum.div(aSum.sum(0))};

        return aSumN.transpose(0, 1);
    }

    // if there's no enough memory, calculate feature probabilities in batches
    torch::Tensor probabilityBatch(const torch::Tensor& db, unsigned int batchSize=1000u) {
        unsigned batches{static_cast<unsigned>(ceil(static_cast<double>(db.size(1)) / static_cast<double>(batchSize)))};
        torch::Tensor pd{torch::empty({0, db.size(0)})};
        for (unsigned b{0u}; b < batches; ++b) {
            std::cout << "\r" << b + 1 << "/" << batches<< std::flush;
            torch::Tensor dbPart{};
            if (b == batches - 1)
                if (db.size(1) % batchSize != 0)
                    dbPart = db.narrow(1, b * batchSize, db.size(1) % batchSize);
                else
                    dbPart = db.narrow(1, b * batchSize, batchSize);
            else
                dbPart = db.narrow(1, b * batchSize, batchSize);

            if (pd.size(0) == 0)
                pd = probability(dbPart);
            else
                pd = torch::cat({pd, probability(dbPart)}, 0);
        }
        std::cout << std::endl;
        return pd;
    }

    torch::Tensor entropy(const torch::Tensor& pd) {
        torch::Tensor t1{pd.clone()};
        torch::Tensor t2{torch::log(pd)};
        t1.resize_({pd.size(0), 1, pd.size(1)});
        t2.resize_({pd.size(0), pd.size(1), 1});
        return -torch::bmm(t1, t2).reshape({pd.size(0), 1});
    }

    torch::Tensor JSD(const torch::Tensor& pd1, const torch::Tensor& pd2) {
        return entropy(pd1.add(pd2).div(2.0)).sub(entropy(pd1).add(entropy(pd2)).div(2.0));
    }

    torch::Tensor featureQuality(const torch::Tensor& db1, const torch::Tensor& db2, const std::string& dest, const std::string& train, const std::string& descName, bool log=true) {
        Timer t{};
        torch::Tensor pd1, pd2;

        if (!fl::in(dest, train + "_prob_query_" + descName + ".pt")) {
            pd1 = probabilityBatch(db1);
            torch::save(pd1, dest + train + "_prob_query_" + descName + ".pt");
            if (log)
                std::cout << train + "_prob_query_" + descName + ".pt built after " << t.secondsByFar() << std::endl;
        } else {
            torch::load(pd1, dest + train + "_prob_query_" + descName + ".pt");
        }

        if (!fl::in(dest, train + "_prob_ref_" + descName + ".pt")) {
            pd2 = probabilityBatch(db2);
            torch::save(pd2, dest + train + "_prob_ref_" + descName + ".pt");
            if (log)
                std::cout << train + "_prob_ref_" + descName + ".pt built after " << t.secondsByFar() << std::endl;
        } else {
            torch::load(pd2, dest + train + "_prob_ref_" + descName + ".pt");
        }

        torch::Tensor res{torch::exp(-JSD(pd1, pd2))};
        // remove all those elements with value 1.0 as they are yielded
        // from uniform distributions (i.e. flat features)
        torch::Tensor remove{res == 1.0};
        remove = remove.toType(torch::ScalarType::Float);
        res.sub_(remove);

        return res;
    }
}

#endif