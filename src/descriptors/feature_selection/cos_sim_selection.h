#ifndef VISUALPLACERECOGNITION_COS_SIM_SELECTION_H
#define VISUALPLACERECOGNITION_COS_SIM_SELECTION_H

#include <torch/torch.h>
#include <descriptors/global/overfeat.h>
#include <AssociationMatrix.h>
#include <utility/img.h>

namespace fsc {

    torch::Tensor featureQuality(const torch::Tensor& db1, const torch::Tensor& db2) {
        auto dbSize{db1.size(0)};
        auto nrFeatures{db1.size(1)};
        torch::Tensor dbc1{db1.clone()};
        torch::Tensor dbc2{db2.clone()};
        dbc1.transpose_(0, 1);
        dbc2.transpose_(0, 1);

        torch::Tensor tn1Norm{torch::norm(dbc1, 2, 1, true)};
        torch::Tensor tn1AddOne{tn1Norm == 0.0f};
        tn1AddOne = tn1AddOne.toType(torch::ScalarType::Float);
        tn1Norm.add_(tn1AddOne);
        dbc1.div_(tn1Norm);

        torch::Tensor tn2Norm{torch::norm(dbc2, 2, 1, true)};
        torch::Tensor tn2AddOne{tn2Norm == 0.0f};
        tn2AddOne = tn2AddOne.toType(torch::ScalarType::Float);
        tn2Norm.add_(tn2AddOne);
        dbc2.div_(tn2Norm);


        dbc1.resize_({nrFeatures, 1, dbSize});
        dbc2.resize_({nrFeatures, dbSize, 1});
        return torch::bmm(dbc1, dbc2).reshape(-1);
    }
}

#endif
