#ifndef VISUALPLACERECOGNITION_SELECTION_H
#define VISUALPLACERECOGNITION_SELECTION_H

#include <iostream>
#include <utility/file.h>
#include <utility/img.h>
#include <utility/tensor.h>
#include <utility/timer.h>
#include <descriptors/feature_selection/shared.h>
#include <descriptors/feature_selection/prob_selection.h>
#include <descriptors/feature_selection/cos_sim_selection.h>

namespace fs {
    void experiment(const std::string& training, const std::string& test, const std::string& descName, const std::string& expName="fsp", const std::string& dbPath="../results/fsp/") {

        float q{0.0f};
        unsigned int c{0u};
        float qArr[20];
        while (q <= 1.0f) {
            qArr[c++] = q;
            q  += 0.05f;
        }

        std::string dest{"../results/" + expName +  "/"};
        std::string imgDest{dest + descName + "/images/" + test + "/"};
        std::string mDest{dest + descName + "/matrices/" + test + "/"};
        fl::createDirectory(dest);
        fl::createDirectory(imgDest);
        fl::createDirectory(mDest);

        std::string trainDBGTPath{"../data/" + training + "_example/gt_" + training + "_example.out"};

        torch::Tensor trainQueryDB{ts::loadPython(dbPath + training + "_query_" + descName + ".pt")};
        torch::Tensor trainRefDB{ts::loadPython(dbPath + training + "_reference_" + descName + ".pt")};
        torch::Tensor trainingExpRefDB{fss::expandRefDB(trainQueryDB, trainRefDB, trainDBGTPath)};


        torch::Tensor testQueryDB{ts::loadPython(dbPath + test + "_query_" + descName + ".pt")};
        torch::Tensor testRefDB{ts::loadPython(dbPath + test + "_reference_" + descName + ".pt")};


        torch::Tensor fQuality{};
        if (expName == "fsp")
            fQuality = fsp::featureQuality(trainQueryDB, trainingExpRefDB, dest, training, descName);
        else if (expName == "fsc")
            fQuality = fsc::featureQuality(trainQueryDB, trainingExpRefDB);


        for (auto q: qArr) {
            if (fl::in(mDest, descName + "_" + test + "_fs_0_dot_" + std::to_string(q).substr(0, 4).replace(1, 1, "_").substr(2, 2) + ".out"))
                std::cout << descName + "_" + test + "_fs_0_dot_" + std::to_string(q).substr(0, 4).replace(1, 1, "_").substr(2, 2) + ".out already saved" << std::endl;
            else {
                Timer t{};
                torch::Tensor queryDBFS{fss::filterDB(testQueryDB, fQuality, q)};
                queryDBFS.div_(torch::norm(queryDBFS, 2, 1, true));

                torch::Tensor refDBFS{fss::filterDB(testRefDB, fQuality, q)};
                refDBFS.div_(torch::norm(refDBFS, 2, 1, true));

                am::AssociationMatrix m{queryDBFS.matmul(refDBFS.transpose(0, 1))};
                std::cout << descName + "_" + test + "_fs_0_dot_" + std::to_string(q).substr(0, 4).replace(1, 1, "_").substr(2, 2) << ": " << t.secondsByFar() << "; f-size: " << queryDBFS.size(1) << std::endl;
                m.saveToFile(mDest + descName + "_" + test + "_fs_0_dot_" + std::to_string(q).substr(0, 4).replace(1, 1, "_").substr(2, 2) + ".out");
                img::saveImage(img::associationMatrixDrawing(m), imgDest + descName + "_" + test + "_fs_0_dot_" + std::to_string(q).substr(0, 4).replace(1, 1, "_").substr(2, 2) + ".png");
            }
        }
    }

    void main(const std::vector<std::tuple<std::string, std::string, std::string>>& experiments, const std::string& expName="fsp", const std::string& dbPath="../results/fsp/") {
        for (const auto& exp: experiments) {
            experiment(std::get<0>(exp), std::get<1>(exp), std::get<2>(exp), expName, dbPath);
            // break;
        }

    }
}

#endif