#ifndef VISUALPLACERECOGNITION_SELECTION_SHARED_H
#define VISUALPLACERECOGNITION_SELECTION_SHARED_H

#include <torch/torch.h>
#include <torch/jit.h>
#include <MatchMatrix.h>
#include <functional>
#include <torch/nn.h>
#include <torch/script.h>
#include <descriptors/global/overfeat.h>

namespace fss {

    void dim(const torch::Tensor& t) {
        std::string s{"("};
        for (auto i{0}; i < t.dim(); ++i)
            s += std::to_string(t.size(i)) + ", ";
        s += "\b\b)\n";
        std::cout << s << std::endl;
    }

    torch::Tensor expandRefDB(const torch::Tensor& queryDB, const torch::Tensor& refDB, const std::string& gtFilePath) {

        unsigned int querySize{static_cast<unsigned int>(queryDB.size(0))};
        unsigned int referenceSize{static_cast<unsigned int>(refDB.size(0))};

        torch::Tensor queryDBN{queryDB};
        queryDBN.div_(torch::norm(queryDBN, 2, 1, true));

        torch::Tensor refDBN{refDB};
        refDBN.div_(torch::norm(refDBN, 2, 1, true));

        mm::MatchMatrix gt{gtFilePath, querySize, referenceSize};
        torch::Tensor gtSimilarities{queryDBN.matmul(refDBN.transpose(0, 1))};
        for (unsigned int i{0}; i < querySize; ++i) {
            const ty::matchMatrixColumn& mmc{gt[static_cast<int>(i)]};
            for (unsigned int j{0}; j < referenceSize; ++j) {
                if (mmc[j] != ty::match::TP)
                    gtSimilarities[i][j] = 0.0f;
            }
        }

        auto fSize{queryDBN.size(1)};
        torch::Tensor tBest{torch::argsort(gtSimilarities, -1, true).transpose(0, 1)[0]};
        tBest = tBest.reshape({tBest.size(0), 1});
        tBest = tBest.repeat({1, fSize});

        return refDB.gather(0, tBest);
    }

    float quantile(const torch::Tensor& fQuality, float q) {
        assert(q >= 0.0f && q <= 1.0f);

        std::tuple<torch::Tensor, torch::Tensor> res{torch::sort(fQuality)};
        torch::Tensor fQualitySorted{std::get<0>(res)};

        const auto n{fQualitySorted.size(0)};
        const auto id{static_cast<float>((n - 1)) * q};
        const auto lo{floorf(id)};
        const auto hi{ceilf(id)};
        const auto qs{fQualitySorted[lo].item<float>()};
        const auto h{id - lo};
        return (1.0f - h) * qs + h * fQualitySorted[hi].item<float>();
    }

    torch::Tensor filterDB(const torch::Tensor& db, const torch::Tensor& fQuality, float q=0.99f) {
        fQuality.resize_({fQuality.size(0)});

        float qValue{quantile(fQuality, q)};

        torch::Tensor dbNew{db.clone()};
        dbNew.transpose_(0, 1);

        torch::Tensor condition{fQuality >= qValue};
        condition.resize_(condition.size(0));
        condition = (condition != 0).nonzero();
        condition = condition.repeat({1, dbNew.size(1)});
        dbNew = dbNew.gather(0, condition);

        return dbNew.transpose(0, 1);

    }

}


#endif