#ifndef VISUALPLACERECOGNITION_VLAD_H
#define VISUALPLACERECOGNITION_VLAD_H

#include <file.h>
#include <img.h>

#include <opencv2/core.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>

#include <descriptors/local/sift.h>
#include <descriptor.h>
#include "descriptors/local/surf.h"

namespace vlad {
    cv::Mat vlad(const cv::Mat& image, const cv::Mat& words);
    std::vector<cv::Mat> parseDatabase(const std::vector<cv::Mat>& descriptors, const cv::Mat& words);
    am::AssociationMatrix createAssociationMatrix(const std::vector<cv::Mat>& queryDBDescriptors, const std::vector<cv::Mat>& referenceDBDescriptors, const cv::Mat& words);
    std::string info(const std::string& wordFilepath);
}

#endif