#include <descriptors/global/bow.h>
#include <descriptors/local/sift.h>

namespace bow {



    std::vector<double> histogram(const cv::Mat& image, const cv::Mat& words) {
        std::vector<double> hist(words.rows, 0.0);
        for (int i{0}; i < image.rows; ++i) {
            cv::Mat descriptor{image.row(i)};
            int nearest{0};
            double nearestDistance{std::numeric_limits<double>::max()};
            for (int j{0}; j < words.rows; ++j) {
                cv::Mat word{words.row(j)};

                double distance{cv::norm(descriptor, word, cv::NORM_L2)};

                if (distance < nearestDistance) {
                    nearestDistance = distance;
                    nearest = j;
                }
            }
            hist[nearest] += 1.0;
        }
        return hist;
    }

    std::vector<std::vector<double>> parseDatabase(const std::vector<cv::Mat>& descriptors, const cv::Mat& words) {
        std::vector<std::vector<double>> db(descriptors.size(), std::vector<double>(words.rows, 0.0));
        unsigned long i{0};
        for (const cv::Mat& desc: descriptors)
            db[i++] = histogram(desc, words);
        return db;
    }

    am::AssociationMatrix createAssociationMatrix(const std::vector<cv::Mat>& queryDBDescriptors, const std::vector<cv::Mat>& referenceDBDescriptors, const cv::Mat& words) {

        am::AssociationMatrix m{};

        Timer t{};

        std::vector<std::vector<double>> queryDB{parseDatabase(queryDBDescriptors, words)};

        //std::cout << "Q: " << t.secondsByFar() << std::endl;

        std::vector<std::vector<double>> referenceDB{parseDatabase(referenceDBDescriptors, words)};

        // std::cout << "R: " << t.secondsByFar() << std::endl;

        for (const std::vector<double>& query: queryDB) {
            ty::associationMatrixColumn column{};
            for (const std::vector<double>& reference: referenceDB)
                column.push_back(af::cosineSimilarityD(query, reference));
            m.container.push_back(column);
        }

        // std::cout << "A: " << t.secondsByFar() << std::endl;

        return m;
    }
}