#include <iostream>
#include <fstream>

#include <descriptors/global/overfeat.h>
#include <file.h>
#include <types.h>
#include <img.h>


namespace overfeat {

    ty::imageRepresentation parseFile(const std::string& path) {
        std::ifstream ifs(path.c_str());
        int n, h, w;
        ifs >> n >> h >> w;
        ty::imageRepresentation imageDescriptor(n * h * w, 0.0f);

        unsigned int index{0u};
        while (!ifs.eof()) {
            float resp;
            ifs >> resp;
            imageDescriptor[index++] = resp;
        }
        return imageDescriptor;
    }

    std::vector<ty::imageRepresentation> parseDatabase(const std::string& path) {
        std::vector<std::string> paths = fl::filePaths(path);
        std::vector<ty::imageRepresentation> db(paths.size());

        unsigned int index{0};
        for(const std::string& filePath: paths) {
            db[index++] = parseFile(filePath);
        }

        return db;
    }

    am::AssociationMatrix createAssociationMatrix(const std::string& queryPath, const std::string& referencePath) {
        std::vector<ty::imageRepresentation> queryDB = parseDatabase(queryPath);
        std::vector<ty::imageRepresentation> referenceDB = parseDatabase(referencePath);
        am::AssociationMatrix m{};
        m.appendAssociationColumns(referenceDB, queryDB);
        return m;
    }

}