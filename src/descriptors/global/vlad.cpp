#include <descriptors/global/vlad.h>

namespace vlad {
    cv::Mat vlad(const cv::Mat& image, const cv::Mat& words) {
        cv::Mat desc(words.rows, words.cols, words.type(), cv::Scalar(0.0));

        for (int i{0}; i < image.rows; ++i) {
            cv::Mat descriptor{image.row(i)};

            int nearestIndex{0};
            double nearestDistance{std::numeric_limits<double>::max()};
            cv::Mat nearestWord{words.row(nearestIndex)};

            for (int j{0}; j < words.rows; ++j) {
                cv::Mat word{words.row(j)};
                double distance{cv::norm(descriptor, word, cv::NORM_L2)};
                if (distance < nearestDistance) {
                    nearestDistance = distance;
                    nearestIndex = j;
                    nearestWord = word;
                }
            }
            desc.row(nearestIndex) += descriptor - nearestWord;
        }
        return desc.reshape(1, words.cols * words.rows);
    }

    std::vector<cv::Mat> parseDatabase(const std::vector<cv::Mat>& descriptors, const cv::Mat& words) {
        std::vector<cv::Mat> db{};
        for (const cv::Mat& desc: descriptors) {
            cv::Mat v{vlad(desc, words)};
            db.push_back(v);
        }

        return db;
    }

    am::AssociationMatrix createAssociationMatrix(const std::vector<cv::Mat>& queryDBDescriptors, const std::vector<cv::Mat>& referenceDBDescriptors, const cv::Mat& words) {

        am::AssociationMatrix m{};

        Timer t{};

        std::vector<cv::Mat> queryDB{parseDatabase(queryDBDescriptors, words)};

        //std::cout << "Q: " << t.secondsByFar() << std::endl;

        std::vector<cv::Mat> referenceDB{parseDatabase(referenceDBDescriptors, words)};

        // std::cout << "R: " << t.secondsByFar() << std::endl;

        for (const cv::Mat& query: queryDB) {
            ty::associationMatrixColumn column{};
            for (const cv::Mat& reference: referenceDB) {
                double ab{query.dot(reference)};
                double aa{query.dot(query)};
                double bb{reference.dot(reference)};
                column.push_back(static_cast<float>(ab / sqrt(aa * bb)));
            }
            m.container.push_back(column);
        }

        // std::cout << "A: " << t.secondsByFar() << std::endl;

        return m;
    }

    std::string info(const std::string& wordFilepath) {
        bool noeps{fl::split(wordFilepath, '_').size() == 6};
        std::string K{fl::split(wordFilepath, '_')[3]};
        std::string N{fl::split(wordFilepath, '_')[5]};
        std::string info = "K_";
        info += K;
        info += "_N_";
        info += N;
        if (!noeps)
            info += "_eps_" + fl::split(wordFilepath, '_')[7];
        info = info.substr(0, info.size() - 4);
        return info;
    }
}