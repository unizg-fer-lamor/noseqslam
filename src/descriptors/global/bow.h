#ifndef VISUALPLACERECOGNITION_BOW_H
#define VISUALPLACERECOGNITION_BOW_H

#include <opencv2/core.hpp> // kmeans
#include <file.h>
#include <descriptor.h>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <timer.h>
#include <limits>


namespace bow {
    std::vector<double> histogram(const cv::Mat& image, const cv::Mat& words);
    std::vector<std::vector<double>> parseDatabase(const std::vector<cv::Mat>& descriptors, const cv::Mat& words);
    am::AssociationMatrix createAssociationMatrix(const std::vector<cv::Mat>& queryDBDescriptors, const std::vector<cv::Mat>& referenceDBDescriptors, const cv::Mat& words);
}

#endif