#ifndef VISUALPLACERECOGNITION_SURF_H
#define VISUALPLACERECOGNITION_SURF_H

#include "opencv2/core.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"

#include <vector>
#include <iostream>
#include <utility/timer.h>

#include "img.h"
#include "file.h"

namespace surf {
    cv::Ptr<cv::xfeatures2d::SURF> initializeSURF(int minHessian = 400);
    std::vector<cv::KeyPoint> keypoints(const cv::Mat& image, const cv::Ptr<cv::xfeatures2d::SURF>& detector);
    cv::Mat descriptors(const cv::Mat& image, std::vector<cv::KeyPoint>& keypoints, const cv::Ptr<cv::xfeatures2d::SURF>& detector);
    void drawKeypoints(const cv::Mat& image, const cv::Ptr<cv::xfeatures2d::SURF>& detector, const std::string& name="surf.png", const std::string& path="../");
    std::vector<cv::Mat> parseDatabase(const std::string& path);
    cv::Mat datasetToPoints(const std::string& dataset="../data/bonn_example/query/images/");
    void visaulizeDataset();
    void bruteForceMatching();
}

#endif