#ifndef VISUALPLACERECOGNITION_SIFT_H
#define VISUALPLACERECOGNITION_SIFT_H

#include <opencv2/core.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>

#include <utility/types.h>
#include <iostream>
#include <utility/img.h>
#include <utility/timer.h>


namespace sift {
    cv::Ptr<cv::SIFT> initializeSIFT();
    std::vector<cv::KeyPoint> keypoints(const cv::Mat& image, const cv::Ptr<cv::SIFT>& detector);
    cv::Mat descriptors(const cv::Mat& image, std::vector<cv::KeyPoint>& keypoints, const cv::Ptr<cv::SIFT>& detector);
    std::vector<cv::Mat> parseDatabase(const std::string& path);
    void drawKeypoints(const cv::Mat& image, const cv::Ptr<cv::SIFT>& detector, const std::string& name="sift.png", const std::string& path="../");
    cv::Mat datasetToPoints(const std::string& dataset="../data/bonn_example/query/images/");
    void visualizeDataset();
    void bruteForceMatching();
}

#endif