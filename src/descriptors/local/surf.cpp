#include "surf.h"

namespace surf {
    cv::Ptr<cv::xfeatures2d::SURF> initializeSURF(int minHessian) {
        return cv::xfeatures2d::SURF::create(minHessian);
    }

    std::vector<cv::KeyPoint> keypoints(const cv::Mat& image, const cv::Ptr<cv::xfeatures2d::SURF>& detector) {
        std::vector<cv::KeyPoint> kp{};
        detector->detect(image, kp);
        return kp;
    }

    cv::Mat descriptors(const cv::Mat& image, std::vector<cv::KeyPoint>& keypoints, const cv::Ptr<cv::xfeatures2d::SURF>& detector) {
        cv::Mat desc;
        detector->compute(image, keypoints, desc);
        return desc;
    }

    void drawKeypoints(const cv::Mat& image, const cv::Ptr<cv::xfeatures2d::SURF>& detector, const std::string& name, const std::string& path) {
        cv::Mat imageWithKeypoints{};
        std::vector<cv::KeyPoint> kp{surf::keypoints(image, detector)};
        cv::drawKeypoints(image, kp, imageWithKeypoints);
        img::saveImage(imageWithKeypoints, path + name);
    }

    std::vector<cv::Mat> parseDatabase(const std::string& path) {
        std::vector<cv::Mat> db(fl::listDirectory(path).size(), cv::Mat{});
        cv::Ptr<cv::xfeatures2d::SURF> detector{initializeSURF()};

        unsigned long i{0u};
        for (const std::string& filepath: fl::listDirectory(path)) {
            cv::Mat image{cv::imread(filepath)};
            std::vector<cv::KeyPoint> kp{keypoints(image, detector)};
            cv::Mat desc{descriptors(image, kp, detector)};
            db[i++] = desc;
        }
        return db;
    }

    void visaulizeDataset() {
        const std::string& newFolder{"../results/surf/test3/"};
        const std::string& dataset{"../data/bonn_example/query/images/"};

        fl::createDirectory(newFolder);
        const std::vector<std::string> paths{fl::filePaths(dataset)};

        cv::Ptr<cv::xfeatures2d::SURF> detector{surf::initializeSURF(600)};



        for (const std::string& filepath: paths) {
            cv::Mat image{cv::imread(filepath)};
            const std::string name{fl::split(filepath, '/')[5]};
            drawKeypoints(image, detector, name, newFolder);
        }
    }

    void bruteForceMatching() {
        am::AssociationMatrix am{};
        const std::string& query{"../data/bonn_example/query/images/"};
        const std::string& reference{"../data/bonn_example/reference/images/"};

        std::vector<cv::Mat> queryDataset{};
        std::vector<cv::Mat> referenceDataset{};

        cv::Ptr<cv::xfeatures2d::SURF> detector{initializeSURF()};

        Timer timer{};



        for (const std::string& path: fl::filePaths(query)) {
            cv::Mat image{cv::imread(path)};
            std::vector<cv::KeyPoint> kp{keypoints(image, detector)};
            cv::Mat desc{descriptors(image, kp, detector)};
            queryDataset.push_back(desc);
        }

        std::cout << timer.secondsByFar() << std::endl;


        for (const std::string& path: fl::filePaths(reference)) {
            cv::Mat image{cv::imread(path)};
            std::vector<cv::KeyPoint> kp{keypoints(image, detector)};
            cv::Mat desc{descriptors(image, kp, detector)};
            referenceDataset.push_back(desc);
        }

        std::cout << timer.secondsByFar() << std::endl;

        std::cout << "Hello\n";

        std::cout << timer.secondsByFar() << std::endl;

        cv::BFMatcher matcher;
        int i{0};
        for (const cv::Mat& queryImage: queryDataset) {
            ty::associationMatrixColumn c{};
            for (const cv::Mat& referenceImage: referenceDataset) {
                std::vector<cv::DMatch> matches;
                matcher.match(queryImage, referenceImage, matches);
                float distance{0.0f};
                for (const cv::DMatch& m: matches) distance += m.distance;
                c.push_back(distance);
            }
            std::cout << ++i << std::endl;
            am.container.push_back(c);
        }

        am.saveToFile("../results/surf/dist_am.out");
        am.normalizeColumns();
        am.invert();
        img::saveImage(img::associationMatrixDrawing(am), "../results/surf/dist1.png");

        std::cout << timer.secondsByFar() << std::endl;
    }

    cv::Mat datasetToPoints(const std::string& dataset) {

        cv::Ptr<cv::xfeatures2d::SURF> detector{surf::initializeSURF()};
        cv::Mat points{};

        for (const std::string& filepath: fl::listDirectory(dataset)) {
            cv::Mat image{cv::imread(filepath)};
            std::vector<cv::KeyPoint> kp{surf::keypoints(image, detector)};
            cv::Mat desc{surf::descriptors(image, kp, detector)};
            points.push_back(desc);
        }
        return points;
    }
}