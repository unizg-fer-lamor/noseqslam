#include "sift.h"

namespace sift {
    cv::Ptr<cv::SIFT> initializeSIFT() {
        return cv::SIFT::create();
    }

    std::vector<cv::KeyPoint> keypoints(const cv::Mat& image, const cv::Ptr<cv::SIFT>& detector) {
        std::vector<cv::KeyPoint> kp{};
        detector->detect(image, kp);
        return kp;
    }

    cv::Mat descriptors(const cv::Mat& image, std::vector<cv::KeyPoint>& keypoints, const cv::Ptr<cv::SIFT>& detector) {
        cv::Mat desc;
        detector->compute(image, keypoints, desc);
        return desc;
    }

    void drawKeypoints(const cv::Mat& image, const cv::Ptr<cv::SIFT>& detector, const std::string& name, const std::string& path) {
        cv::Mat imageWithKeypoints{};
        std::vector<cv::KeyPoint> kp{sift::keypoints(image, detector)};
        cv::drawKeypoints(image, kp, imageWithKeypoints);
        img::saveImage(imageWithKeypoints, path + name);
    }

    std::vector<cv::Mat> parseDatabase(const std::string& path) {
        std::vector<cv::Mat> db(fl::listDirectory(path).size(), cv::Mat{});
        cv::Ptr<cv::SIFT> detector{initializeSIFT()};

        unsigned long i{0u};
        for (const std::string& filepath: fl::listDirectory(path)) {
            cv::Mat image{cv::imread(filepath)};
            std::vector<cv::KeyPoint> kp{keypoints(image, detector)};
            cv::Mat desc{descriptors(image, kp, detector)};
            db[i++] = desc;
        }
        return db;
    }

    void visualizeDataset() {
        std::string dataset{"../data/freiburg_example/query/images/"};
        std::string path{"../results/sift/test1/"};
        fl::createDirectory(path);

        cv::Ptr<cv::SIFT> detector{initializeSIFT()};

        for (const std::string& filepath: fl::listDirectory(dataset)) {
            std::string filename{fl::split(filepath, '/')[5]};
            cv::Mat image{cv::imread(filepath)};
            drawKeypoints(image, detector, filename, path);
        }
    }


    cv::Mat datasetToPoints(const std::string& dataset) {

        cv::Ptr<cv::SIFT> detector{sift::initializeSIFT()};
        cv::Mat points{};

        for (const std::string& filepath: fl::listDirectory(dataset)) {
            cv::Mat image{cv::imread(filepath)};
            std::vector<cv::KeyPoint> kp{sift::keypoints(image, detector)};
            cv::Mat desc{sift::descriptors(image, kp, detector)};
            points.push_back(desc);
        }
        return points;
    }
}