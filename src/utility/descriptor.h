#ifndef VISUALPLACERECOGNITION_DESCRIPTOR_H
#define VISUALPLACERECOGNITION_DESCRIPTOR_H

#include <opencv2/core.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>

namespace desc {
    void training(const cv::Mat& points, int K, const std::string& path, int N=10, float eps=1.0, int attempts=10);
}

#endif