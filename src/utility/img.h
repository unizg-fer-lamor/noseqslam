//
// Created by jurica on 08/04/19.
//

#ifndef VISUALPLACERECOGNITION_IMG_H
#define VISUALPLACERECOGNITION_IMG_H

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <AssociationMatrix.h>
#include <MatchMatrix.h>

namespace img {
    void showImage(const cv::Mat& image, const std::string& title="Image");
    void saveImage(const cv::Mat& image, const std::string& dest);
    cv::Mat associationMatrixDrawing(const am::AssociationMatrix& m, const int squareSize=2);
    cv::Mat matchMatrixDrawing(const mm::MatchMatrix& n, const int squareSize=2);
    cv::Mat appendLegend(cv::Mat& img, std::vector<std::pair<std::string, cv::Scalar>> items);

}

#endif //VISUALPLACERECOGNITION_IMG_H
