#include <descriptor.h>
#include <file.h>

namespace desc {
    void training(const cv::Mat& points, int K, const std::string& path, int N, float eps, int attempts) {
        cv::Mat words, labels;
        cv::kmeans(points, K, labels, cv::TermCriteria{cv::TermCriteria::COUNT, N, eps}, attempts, cv::KMEANS_PP_CENTERS, words);
        fl::saveMat(words, path);
    }
}