//
// Created by jurica on 08/04/19.
//

#include <file.h>
#include <iostream>
#include <boost/algorithm/string.hpp>

namespace fl {
    std::vector<std::string> filePaths(const std::string& path) {
        // folder items iterator
        boost::filesystem::directory_iterator it = boost::filesystem::directory_iterator{path};

        // cast paths to vector in order to sort them
        std::vector<std::string> paths{};

        for(const auto& el: it)
            paths.push_back(el.path().string());

        // sort path by name
        std::sort(paths.begin(), paths.end());

        return paths;
    }

    void createDirectory(const std::string& path) {
        boost::filesystem::create_directories(path);
    }

    void savePrecisionAndRecall(const std::string& path, const std::string& fileName, const std::vector<std::pair<float, float>>& values) {
        std::ofstream file;
        file.open(path + fileName);
        unsigned int index{0u};
        for(const std::pair<float, float>& value: values)
            file << std::to_string(++index) << " " << std::to_string(value.first) << " " << std::to_string(value.second) << "\n";

        file.close();
    }

    std::vector<std::string> listDirectory(const std::string& path) {
        std::vector<std::string> paths;
        boost::filesystem::path p{path};
        std::vector<boost::filesystem::directory_entry> v; // To save the file names in a vector.

        if(boost::filesystem::is_directory(p))
        {
            copy(boost::filesystem::directory_iterator(p), boost::filesystem::directory_iterator(), back_inserter(v));

            for (std::vector<boost::filesystem::directory_entry>::const_iterator it = v.begin(); it != v.end();  ++ it)
                paths.push_back((*it).path().string());
        }
        std::sort(paths.begin(), paths.end());
        return paths;
    }

    std::vector<std::string> listDirectoryNames(const std::string& path) {
        std::vector<std::string> pathsFull{listDirectory(path)};
        std::vector<std::string> names{};
        for(const std::string& p: pathsFull)
            names.push_back(split(p, '/').back());
        return names;
    }


    std::vector<std::string> split(const std::string& text, char delimiter) {
        std::vector<std::string> results;
        boost::split(results, text, [&delimiter](char c){return c == delimiter;});
        return results;
    }

    std::string extension(const std::string& text) {
        return split(text, '.').back();
    }

    void saveMat(const cv::Mat& value, const std::string& filepath) {
        std::string extension{fl::split(fl::split(filepath, '/').back(), '.').back()};

        if ( extension != "xml" && extension != "yml" ) {
            std::cout << "Not saved due to the extension ." << extension << std::endl;
            return;
        }

        cv::FileStorage fs{filepath, cv::FileStorage::WRITE};
        fs << "value" << value;
        fs.release();
    }

    bool in(const std::string& folder, const std::string& file) {
        std::vector<std::string> names{listDirectoryNames(folder)};
        return std::count(names.begin(), names.end(), file);
    }

    std::string join(const std::vector<std::string>& s,  const std::string& delimiter) {
        std::string str{""};
        for (unsigned int i{0u}; i < s.size(); ++i) {
            str += s[i];
            if (i != s.size() - 1)
                str += delimiter;
        }
        return str;
    }

    std::string folder(const std::string& path) {
        auto p{split(path, '/')};
        p.pop_back();
        return join(p, "/");
    }

    cv::Mat loadMat(const std::string& filepath) {
        cv::Mat words{};
        cv::FileStorage fs{filepath, cv::FileStorage::READ};
        fs["value"] >> words;
        return words;
    }

    std::vector<std::string> woLast(const std::vector<std::string>& v) {
        std::vector<std::string> wo{v};
        wo.pop_back();
        return wo;
    }

}

