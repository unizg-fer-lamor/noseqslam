//
// Created by jurica on 08/04/19.
//

#include <img.h>

namespace img {

    void showImage(const cv::Mat& image, const std::string& title) {
        namedWindow(title, cv::WINDOW_AUTOSIZE );
        imshow(title, image);
        cv::waitKey(0);
    }

    void saveImage(const cv::Mat& image, const std::string& dest) {
        std::vector<int> compression_params;
        compression_params.push_back(cv::IMWRITE_PNG_COMPRESSION);
        compression_params.push_back(9);
        bool result = false;
        try {
            result = imwrite(dest, image, compression_params);
        }
        catch (const cv::Exception& ex) {
            fprintf(stderr, "Exception converting image to PNG format: %s\n", ex.what());
        }
        if (result)
            0; // printf("Saved PNG file with alpha data.\n");
        else
            printf("ERROR: Can't save PNG file.\n");
    }


    cv::Mat associationMatrixDrawing(const am::AssociationMatrix& m, const int squareSize) {
        const int h = squareSize * m.referenceLength();
        const int w = squareSize * m.queryLength();
        cv::Mat greyScale{h, w, CV_8UC1, cv::Scalar{255}};
        for(int i = 0; i < m.referenceLength(); ++i)
            for(int j = 0; j < m.queryLength(); ++j) {
                const float association = m.container[j][i];
                cv::rectangle(greyScale, cv::Point{j * squareSize, i * squareSize}, cv::Point{(j + 1) * squareSize, (i + 1) * squareSize}, cv::Scalar{association * 255}, cv::FILLED);
            }
        return greyScale;
    }

    cv::Mat matchMatrixDrawing(const mm::MatchMatrix &n, const int squareSize) {

        const int h = squareSize * n.referenceLength();
        const int w = squareSize * n.queryLength();
        cv::Mat image{h, w, CV_8UC3, cv::Scalar{255, 255, 255}};
        for(int i = 0; i < n.referenceLength(); ++i)
            for(int j = 0; j < n.queryLength(); ++j) {
                ty::match matchType = n[j][i];
                cv::Scalar currentColor;
                if (matchType == ty::match::FN) currentColor = ty::colorFN;
                else if (matchType == ty::match::FP) currentColor = ty::colorFP;
                else if (matchType == ty::match::TP) currentColor = ty::colorTP;
                else currentColor = ty::colorTN;
                cv::rectangle(image, cv::Point{j * squareSize, i * squareSize}, cv::Point{(j + 1) * squareSize, (i + 1) * squareSize}, currentColor, cv::FILLED);
            }
        return image;
    }

    cv::Mat appendLegend(cv::Mat &img, std::vector<std::pair<std::string, cv::Scalar>> items) {
        std::string label = items[0].first;
        int height = img.rows;
        int width = img.cols;

        int rowHeight = 20;
        int padding = 10;
        int rowWidth = 100;
        int nrOfRows = items.size();

        int x0 = padding;
        int y0 = height - nrOfRows * rowHeight - padding;
        int x1 = padding + rowWidth;
        int y1 = height - padding;

        cv::rectangle(img, cv::Point{ x0, y0 }, cv::Point{ x1, y1 }, cv::Scalar{255, 255, 255}, cv::FILLED);

        int index = 0u;
        for(auto item: items) {

            int x0_i = x0;
            int y0_i = y0 + (index++) * 19 + 15;

            int squareSize = 10;
            int squareShiftY = -10;
            int squareShiftX = 5;

            cv::rectangle(img, cv::Point{ x0_i + squareShiftX, y0_i + squareShiftY }, cv::Point{ x0_i + squareShiftX + squareSize, y0_i + squareSize + squareShiftY }, item.second, cv::FILLED);
            cv::putText(img, item.first, cv::Point{ x0_i + 20, y0_i }, cv::FONT_HERSHEY_SIMPLEX, 0.5, CV_RGB(0,0,0), 1);
        }

        return img;
    }

}



