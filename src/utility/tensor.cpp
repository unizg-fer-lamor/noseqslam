#include "tensor.h"
#include "file.h"
#include <utility/timer.h>
#include <utility/img.h>
#include <descriptors/global/overfeat.h>

namespace ts {
    // load from python
    torch::Tensor loadPython(const std::string& path) {

        torch::jit::script::Module m{torch::jit::load(path)};
        for (const auto& el: m.named_parameters()) {
            torch::NoGradGuard guard;
            return el.value.data();
        }


    }

    // save for python
    void savePython(const torch::Tensor& t, const std::string& path) {
        auto bytes = torch::jit::pickle_save(t);
        std::ofstream file(path, std::ios::out | std::ios::binary);
        file.write(bytes.data(), bytes.size());
        file.close();
    }

    void print(const torch::Tensor& t) {
        std::cout << "(";
        for (auto i: t.sizes())
            std::cout << t.size(i) << ",";
        std::cout << "\b)";
    }
    /*
    torch::Tensor db2Tensor(const std::string& path, const std::string& destination, const std::function<std::vector<std::vector<float>>(const std::string&)>& parseDatabase=overfeat::parseDatabase) {

        std::string name{fl::split(destination, '/').back()};
        std::string folder{fl::folder(destination)};

        if (fl::in(folder, name)) {
            torch::Tensor t{};
            torch::load(t, destination);
            return t;
        }

        std::vector<ty::imageRepresentation> db{parseDatabase(path)};
        long dbSize{static_cast<long>(db.size())};
        long fSize{static_cast<long>(db[0].size())};

        torch::Tensor t{torch::zeros({ dbSize, fSize})};
        for (auto i{0l}; i < dbSize; ++i)
            t[i] = torch::from_blob(db[i].data(), {fSize});

        torch::save(t, destination);

        return t;
    }

    void main()  {

        std::tuple<std::string, std::string, std::string> experiments[] = {
                { "overfeat10", "freiburg", "query" },
                { "overfeat10", "freiburg", "reference" },
                { "overfeat10", "bonn", "query" },
                { "overfeat10", "bonn", "reference" }
        };

        for (const auto& e: experiments) {
            std::string desc{std::get<0>(e)};
            std::string dataset{std::get<1>(e)};
            std::string database{std::get<2>(e)};

            std::string path{"../data/"+ dataset + "_example_new/" + database + "/OverFeat_features/"};
            std::string dest{"../results/fsp/" + dataset + "_" + database + "_" + desc + ".pt"};

            Timer t{};
            db2Tensor(path, dest);
            std::cout << "Time elapsed: " << t.secondsByFar() << std::endl;
        }
    }*/

}