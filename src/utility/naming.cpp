#include "naming.h"

namespace name {

    std::ostream& operator<<(std::ostream& os, const Info& i) {
        return os << "Method: " << i.method << "; " << "descriptors: " << i.descriptors << "; K: " << i.K << "; " << "N: " << i.N << "; DB: " << i.db << "; FS: " << i.fs << std::endl;
    }

    // info for VLAD and BOW
    Info info1(const std::string& filepath) {
        std::string filename{fl::split(filepath, '/').back()};
        std::string folder{fl::split(filepath, '/').end()[-3]};
        unsigned int K{static_cast<unsigned int>(std::stoi(fl::split(filename, '_')[3]))};
        unsigned int N{static_cast<unsigned int>(std::stoi(fl::split(filename, '_')[5]))};
        std::string method{folder};
        std::string descriptors{fl::split(filename, '_')[1]};
        return Info{method, descriptors, K, N};
    }

    // info for SIFT and SURF (BF)
    Info info2(const std::string& filepath) {
        std::string filename{fl::split(filepath, '/').back()};
        std::string method{"BF"};
        std::string descriptors{fl::split(filename, '_')[1]};
        return Info{method, descriptors, 0, 0};
    }

    // info for fsc, fsp
    Info info3(const std::string& filepath) {
        std::string filename{fl::split(filepath, '/').back()};
        std::string method{fl::split(filepath, '/')[3]};
        std::string db{fl::split(filename, '_')[1]};
        std::string fs{fl::split(fl::split(filename, '_').back(), '.').front()};
        return Info{method, "", 0, 0, db, fs};
    }

    Info info4(const std::string& filepath) {
        std::string filename{fl::split(filepath, '/').back()};
        std::string method{fl::split(filepath, '/')[3]};
        std::string db{fl::split(filename, '_')[1]};
        std::string fs{fl::split(fl::split(filename, '_').back(), '.').front()};
        return Info{method, "", 0, 0, db, ""};
    }

    Info info5(const std::string& filepath) {
        std::string filename{fl::split(filepath, '/').back()};

        std::vector<std::string> woExtensionVector{fl::split(filename, '.')};
        woExtensionVector.pop_back();
        std::string withoutExtension{fl::join(woExtensionVector, ".")};
        std::string method{fl::split(withoutExtension, '_')[0]};
        std::string db{fl::split(withoutExtension, '_')[1]};
        return Info{method, "", 0, 0, db, ""};
    }


    // info for VLAD and BOW
    std::string info2Filename1(const Info& info, unsigned int ds, int etaExp, const std::string& algorithm) {
        return info.method + "_" + info.descriptors + "_K_" + std::to_string(info.K) + "_N_" + std::to_string(info.N) + "_" + "Bonn" + "_" + std::to_string(ds) + "_" + std::to_string(etaExp) + "_" + algorithm;
    }

    // info for fsc, fsp
    std::string info2Filename2(const Info& info, unsigned int ds, int etaExp, const std::string& algorithm) {
        return info.method + "_" + info.db + "_" + info.fs + "_" + std::to_string(ds) + "_" + std::to_string(etaExp) + "_" + algorithm;
    }

    // info for fsc, fsp, another variant
    std::string info2Filename3(const Info& info, unsigned int ds, int etaExp, const std::string& algorithm, bool withoutFolder) {
        if (!withoutFolder)
            return info.db + "/" + info.method + "_" + info.db + "_" + info.fs + "_" + std::to_string(ds) + "_" + std::to_string(etaExp) + "_" + algorithm;
        else return info.method + "_" + info.db + "_" + info.fs + "_" + std::to_string(ds) + "_" + std::to_string(etaExp) + "_" + algorithm;
    }

    // info for metric learning
    std::string info2Filename5(const Info& info, unsigned int ds, int etaExp, const std::string& algorithm, bool withoutFolder) {
        if (!withoutFolder)
            return info.db + "/" + info.method + "_" + info.db + "_" + std::to_string(ds) + "_" + std::to_string(etaExp) + "_" + algorithm;
        else return info.method + "_" + info.db + "_" + std::to_string(ds) + "_" + std::to_string(etaExp) + "_" + algorithm;
    }

    unsigned int total(const std::vector<std::string>&folders, const std::vector<unsigned int>& dsArr, const std::vector<int>& etaExpArr) {
        unsigned int t{0};
        for (const std::string& folder: folders)
            for (const std::string& filepath: fl::listDirectory(folder)) {
                if (fl::extension(filepath) != "out") continue;
                for (unsigned int ds: dsArr)
                    for (int etaExp: etaExpArr)
                        ++t;
            }

        return t;
    }

    unsigned int total(const std::vector<std::string>&folders, const std::vector<unsigned int>& dsArr) {
        unsigned int t{0};
        for (const std::string& folder: folders)
            for (const std::string& filepath: fl::listDirectory(folder)) {
                if (fl::extension(filepath) != "out") continue;
                for (unsigned int ds: dsArr)
                    ++t;
            }
        return t;
    }
}