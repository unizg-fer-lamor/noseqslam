#ifndef VISUALPLACERECOGNITION_NAMING_H
#define VISUALPLACERECOGNITION_NAMING_H

#include <iostream>
#include <utility/file.h>

namespace name {
    struct Info {
        std::string method;
        std::string descriptors;
        unsigned int K;
        unsigned int N;
        std::string db;
        std::string fs;
        friend std::ostream& operator<<(std::ostream& os, const Info& i);
    };

    // info for VLAD and BOW
    Info info1(const std::string& filepath);

    // info for SIFT and SURF (BF)
    Info info2(const std::string& filepath);

    // info for fsc, fsp
    Info info3(const std::string& filepath);

    // info for ADS (DELF & Radenovic et al.) and MZPSIV
    Info info4(const std::string& filepath);

    // info for metric learning
    Info info5(const std::string& filepath);

    // info for VLAD and BOW
    std::string info2Filename1(const Info& info, unsigned int ds, int etaExp, const std::string& algorithm);

    // info for fsc, fsp
    std::string info2Filename2(const Info& info, unsigned int ds, int etaExp, const std::string& algorithm);

    // info for fsc, fsp, and MZPSIV
    std::string info2Filename3(const Info& info, unsigned int ds, int etaExp, const std::string& algorithm, bool withoutFolder=false);

    // info for ADS (DELF & Radenovic et al.)
    std::string info2Filename4(const Info& info, unsigned int ds, int etaExp, const std::string& algorithm);

    // info for metric learning
    std::string info2Filename5(const Info& info, unsigned int ds, int etaExp, const std::string& algorithm, bool withoutFolder=false);


    template <ssize_t F, ssize_t D, ssize_t E>
    unsigned int total(const std::string(&folders)[F], const unsigned int (&dsArr)[D], const int (&etaExpArr)[E]) {
        unsigned int t{0};
        for (const std::string& folder: folders)
            for (const std::string& filepath: fl::listDirectory(folder)) {
                if (fl::extension(filepath) != "out") continue;
                for (unsigned int ds: dsArr)
                    for (int etaExp: etaExpArr)
                        ++t;
            }

        return t;
    }

    template <ssize_t F, ssize_t D>
    unsigned int total(const std::string(&folders)[F], const unsigned int (&dsArr)[D]) {
        unsigned int t{0};
        for (const std::string& folder: folders)
            for (const std::string& filepath: fl::listDirectory(folder)) {
                if (fl::extension(filepath) != "out") continue;
                for (unsigned int ds: dsArr)
                    ++t;
            }
        return t;
    }

    unsigned int total(const std::vector<std::string>&folders, const std::vector<unsigned int>& dsArr, const std::vector<int>& etaExpArr);

    unsigned int total(const std::vector<std::string>&folders, const std::vector<unsigned int>& dsArr);
}

#endif