#ifndef VISUALPLACERECOGNITION_TIMER_H
#define VISUALPLACERECOGNITION_TIMER_H

#include <ctime>

struct Timer {
private:
    clock_t begin;
public:
    Timer();
    ~Timer();
    double secondsByFar() const;
};

#endif