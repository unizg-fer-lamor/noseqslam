#ifndef VISUALPLACERECOGNITION_TENSOR_H
#define VISUALPLACERECOGNITION_TENSOR_H

#include <torch/torch.h>
#include <torch/script.h>
#include <file.h>

namespace ts {
    torch::Tensor loadPython(const std::string& path);
    void savePython(const torch::Tensor& t, const std::string& path);
    void print(const torch::Tensor& t);
    // torch::Tensor db2Tensor(const std::string& path, const std::string& destination, const std::function<std::vector<std::vector<float>>(const std::string&)>& parseDatabase);

    // void main();

}

#endif