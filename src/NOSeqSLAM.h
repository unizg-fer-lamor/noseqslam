//
// Created by jurica on 4/2/19.
//

#ifndef VISUALPLACERECOGNITION_NOSEQSLAM_H
#define VISUALPLACERECOGNITION_NOSEQSLAM_H

#include <AssociationMatrix.h>

namespace noseq {
    float shortestPathAssociation(const am::AssociationMatrix& m, am::AssociationMatrix& dist, am::AssociationMatrix& sim, unsigned int i, unsigned int j, unsigned d_s, int expRate);
    am::AssociationMatrix noSeqSLAM(const am::AssociationMatrix& m, unsigned int d_s, int expRate);
    void main(const std::vector<std::string>& folders, const std::vector<unsigned int>& dsArr, const std::vector<int>& etaExpArr, const std::string& expName);
    void runningTimes();
}



#endif