# NOSeqSLAM implementation #

### Requirements ###

In order to build and run this implementation the following packages are required:

* CMake (ver 3.15)
* OpenCV (ver 4.1.1)
* Boost (ver 1.65.1)
* Torch (ver 1.2)


### Build & run ###

1. cd noseqslam
2. mkdir build && cd build
3. cmake -DCMAKE_PREFIX_PATH="/path/to/libtorch/" ..
4. make -j7
5. ./noseqslam

### Commits ###

* **[03/09/2019]** 811447efd94028b44d1d3f3270b3841775b17694
    * This is code for [NOSeqSLAM paper](https://link.springer.com/chapter/10.1007/978-3-030-35990-4_15) presented in ROBOT'2019 conference.
    
* **[06/01/2020]** 4f59c6f021c8806b35a069c47af632f27f3196d5 
    * Feature selection implemented

* **[28/02/2020]** 44d8517f6373de08ac6f681a306bd08ebc6814f3 
    * BOW and VLAD methods (alongside SIFT and SURF) implemented
  
* **[25/02/2021]** HEAD
    * The newest commit implements `softmaxExperiment()` that evaluates sequence based algorithms given representation obtained with [the softmax regression](https://bitbucket.org/unizg-fer-lamor/vpr-softmax/src/master/).
    
### Datasets ###

By far, in this implementation Freiburg and Bonn datasets have been used.
These are the publicly available datasets of the corresponding paper "Lazy Data Association for Image Sequences Matching Under Substantial Appearance Changes" by Vysotska et al.
Go to [this link](https://github.com/PRBonn/online_place_recognition) in order to obtain datasets.